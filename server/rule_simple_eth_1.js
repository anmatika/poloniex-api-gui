module.exports = {
  name: 'ETH_SIMPLE_1',
  coin: 'BTC_ETH',
  actions: [
    {
      action: 'buy',
      amount: 0.001,
      oneShot: true,
      conditions: [{
        valueType: '*',
        valueCompare: '>',
        value: 1.01,
        timeSpanMinutes: 720,
      }],
    }, {
      action: 'sell',
      amount: 0.001,
      oneShot: true,
      conditions: [{
        valueType: '*',
        valueCompare: '<',
        value: 0.99,
        timeSpanMinutes: 720,
      }],
    }],
};
