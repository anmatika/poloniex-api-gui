const ruleEth1 = require('./rule_eth_1');
const ruleSimpleEth = require('./rule_simple_eth_1');
const ruleSimpleDash = require('./rule_simple_dash_1');
const ruleSimpleGnt = require('./rule_simple_gnt_1');

module.exports = [
  ruleSimpleEth,
];

