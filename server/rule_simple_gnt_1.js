module.exports = {
  name: 'GNT_SIMPLE_1',
  coin: 'BTC_GNT',
  actions: [
    {
      action: 'buy',
      amount: 0.001,
      conditions: [{
        valueType: '*',
        valueCompare: '>',
        value: 1.01,
        timeSpanMinutes: 1720,
      }],
    }, {
      action: 'sell',
      amount: 0.001,
      conditions: [{
        valueType: '*',
        valueCompare: '<',
        value: 0.99,
        timeSpanMinutes: 1720,
      }],
    }],
};
