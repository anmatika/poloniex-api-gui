module.exports = {
  name: 'DASH_SIMPLE_1',
  coin: 'BTC_DASH',
  actions: [
    {
      action: 'buy',
      amount: 0.001,
      conditions: [{
        valueType: '*',
        valueCompare: '>',
        value: 1.03,
        timeSpanMinutes: 720,
      }],
    }, {
      action: 'sell',
      amount: 0.001,
      conditions: [{
        valueType: '*',
        valueCompare: '<',
        value: 0.97,
        timeSpanMinutes: 720,
      }],
    }],
};
