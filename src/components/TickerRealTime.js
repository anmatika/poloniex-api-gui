import React from 'react';
import { connect } from 'react-redux';
import { Button, Panel, ListGroup, ListGroupItem } from 'react-bootstrap';
import SearchInput, { createFilter } from 'react-search-input';
import classNames from 'classnames';
import * as actions from '../actions/poloniex';

const TickerRealTime = ({ state, dispatch }) => {
  function onClick(e) {
    dispatch(actions.startTickerRealTimeAsync());
  }

  const searchUpdated = (term) => {
    dispatch(actions.setTickerRealTimeSearchTerm(term || ''));
  };

  const KEYS_TO_FILTERS = ['key'];
  const filteredTickersRealTime = state.tickersRealTime
  .filter(createFilter(state.tickersRealTimeSearchTerm, KEYS_TO_FILTERS));

  return (
    <div>
      <h2>Ticker stream</h2>
      <div
        className="row"
      >
        <div className="col-xs-12 margin-b-25">
          <Button bsStyle="primary" onClick={onClick} >
            { state.showTickerRealTimeSubscribed
              ? 'Disconnect stream'
              : 'Connect stream'
            }
          </Button>
        </div>
        <div className="col-xs-12 margin-b-25">
          <SearchInput className="search-input" onChange={searchUpdated} />
        </div>
      </div>
      { state.tickersRealTime.length > 0 && filteredTickersRealTime.map((x) => {
        const priceClassNames = classNames({
          'color-green': x.value.priceChangedUp,
          'color-red': x.value.priceChangedDown,
          'color-black': x.value.priceSame,
        });

        return (<div key={`ticker-${x.key}`}>
          <div className="col-xs-3">
            <Panel header={x.key} className="ticker-realtime-panel">
              <ListGroup>
                <ListGroupItem>
              Price: <span className={priceClassNames}>{ x.value.lastPrice }</span>
                </ListGroupItem>
                <ListGroupItem>
              %: { x.value.percentChange }
                </ListGroupItem>
                <ListGroupItem>
              Volume (base): { x.value.baseVolume }
                </ListGroupItem>
                <ListGroupItem>
              Volume (quote): { x.value.quoteVolume }
                </ListGroupItem>
                <ListGroupItem>
              High (24h): { x.value.high24 }
                </ListGroupItem>
                <ListGroupItem>
              Low (24h): { x.value.low24 }
                </ListGroupItem>
              </ListGroup>
            </Panel>
          </div>
        </div>);
      })}
    </div>);
};

export default connect(state => ({ state: state.app }))(TickerRealTime);
