import React from 'react';
import { connect } from 'react-redux';
import { Label, Button, Panel, ListGroup, ListGroupItem } from 'react-bootstrap';
import classNames from 'classnames';
import * as actions from '../actions/poloniex';
import 'font-awesome/css/font-awesome.css';

const Rules = ({ state, dispatch }) => {
  const onClick = () => {
    dispatch(actions.getRulesAsync());
  };

  const onClickStartRules = () => {
    if (!state.showTickerRealTimeSubscribed) {
      dispatch(actions.startTickerRealTimeAsync());
      dispatch(actions.toggleAutoTradeByRules(true));
      return;
    }
    dispatch(actions.toggleAutoTradeByRules(!state.autoTrading.isOn));
  };

  const Conditions = props => (
    <ListGroup className="rules-condition-list-group">
      <ListGroupItem>
        <Label bsStyle="warning"> ValueType:</Label> { props.valueType }
      </ListGroupItem>
      <ListGroupItem>
        <Label bsStyle="warning"> ValueCompare:</Label> { props.valueCompare }
      </ListGroupItem>
      <ListGroupItem>
        <Label bsStyle="warning"> Value:</Label> { parseFloat(Math.round(props.value * 1000) / 1000).toFixed(3) }
      </ListGroupItem>
      <ListGroupItem>
        <Label bsStyle="danger"> TimeSpanMinutes:</Label> { props.timeSpanMinutes }
      </ListGroupItem>
    </ListGroup>);

  const PanelHeaderBuy = (props) => {
    console.log('props', props);
    const tradingEngineResult = state.tradingEngineResult || { buyData: {} };

    return (
      <div>
        <div className="label label-success"> buy { props.amount } pcs </div>
        <div>One shot: { props.oneShot ? 'Yes' : 'No' } </div>
        <div>Cut off: { tradingEngineResult.buyData.priceCutOff } </div>
      </div>);
  };

  const PanelHeaderSell = (props) => {
    const tradingEngineResult = state.tradingEngineResult || { sellData: {} };

    return (
      <div>
        <div className="label label-danger"> sell { props.amount } pcs </div>
        <div>One shot: { props.oneShot ? 'Yes' : 'No' } </div>
        <div>Cut off: { tradingEngineResult.sellData.priceCutOff } </div>
      </div>);
  };

  const PanelRuleHeader = (props) => {
    const tradingEngineResult = state.tradingEngineResult || {};
    const priceMovementClass = classNames({
      'color-green': tradingEngineResult.priceMovement > 0,
      'color-red': tradingEngineResult.priceMovement < 0,
      'color-black': tradingEngineResult.priceMovement === 0,
    });
    return (<div>
      <div>Rule name: {props.name}</div>
      <div>Coin: {props.coin}</div>
      <div>Run time: { tradingEngineResult.runningTimeInMinutes } mins</div>
      <div>Start price: { tradingEngineResult.startPrice } </div>
      <div>Current price: { tradingEngineResult.lastPrice } </div>
      <div>Movement: <span className={priceMovementClass}>{ tradingEngineResult.priceMovement || 0 } %</span></div>
    </div>);
  };

  return (<div>
    <h2>Rules </h2>
    <div
      className="row"
    >
      <div className="col-xs-12 margin-b-25">
        <div className="row margin-b-25">
          <Button bsStyle="primary" onClick={onClick} >
            Get Rules
          </Button>
        </div>
        <div className="row">
          <Button bsStyle={state.autoTrading.isOn ? 'danger' : 'primary'} onClick={onClickStartRules} >
            { state.autoTrading.isOn ? 'Stop trading' : 'Auto trade' }
          </Button>
        </div>
      </div>
    </div>
    <div />

    { state.rules.map(x => (<div key={`rule-${x.name}`}>
      <div className="col-xs-12">
        <Panel header={<PanelRuleHeader {...x} />} className="rule-panel">
          {
              x.actions.map(a => (
                <div className="col-xs-4">
                  <Panel
                    header={a.action === 'buy' ? <PanelHeaderBuy {...a} /> : <PanelHeaderSell {...a} />}
                  >
                    {
                    a.conditions.map(c => <Conditions {...c} />)
                  }
                  </Panel>
                </div>
              ))
             }
        </Panel>
      </div>
    </div>))
    }
  </div>);
};

export default connect(state => ({ state: state.app }))(Rules);
