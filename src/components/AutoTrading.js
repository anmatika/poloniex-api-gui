import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions/poloniex';
import Rules from './Rules';

const AutoTrading = ({ state, dispatch }) => {
  if (state.autoTrading.isOn && !state.autoTrading.allCoinsInStream) {
    setTimeout(() => {
      dispatch(actions.toggleAutoTradeByRules(true));
    }, 1000);
  }

  return (
    <Rules />
  );
};

export default connect(state => ({ state: state.app }))(AutoTrading);
