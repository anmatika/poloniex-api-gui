import objectHelper from '../utils/objectHelper';
import Client from '../utils/Client';
import { batchActions } from 'redux-batched-actions';
import service from '../service';

export const GET_BALANCES = 'GET_BALANCES';
export const SHOW_TICKER = 'SHOW_TICKER';
export const SHOW_TICKER_REAL_TIME = 'SHOW_TICKER_REAL_TIME';
export const SHOW_OPEN_ORDERS = 'SHOW_OPEN_ORDERS';
export const SET_INITIAL_VALUES = 'SET_INITIAL_VALUES';
export const BUY = 'BUY';
export const SHOW_MESSAGE = 'SHOW_MESSAGE';
export const TOGGLE_SPINNER = 'TOGGLE_SPINNER';
export const SHOW_TICKER_REAL_TIME_SUBSCRIBED = 'SHOW_TICKER_REAL_TIME_SUBSCRIBED';
export const SET_TICKER_REAL_TIME_SEARCH_TERM = 'SET_TICKER_REAL_TIME_SEARCH_TERM';
export const GET_RULES = 'GET_RULES';
export const TOGGLE_AUTOTRADE_BY_RULES = 'TOGGLE_AUTOTRADE_BY_RULES';
export const SET_TRADING_ENGINE_RESULT = 'SET_TRADING_ENGINE_RESULT';


export function setInitialValues() {
  return {
    type: SET_INITIAL_VALUES,
  };
}

export function getBalances(data) {
  return {
    type: GET_BALANCES,
    data,
  };
}

export function showTicker(data) {
  return {
    type: SHOW_TICKER,
    data,
  };
}

export function showTickerRealTime(data) {
  return {
    type: SHOW_TICKER_REAL_TIME,
    data,
  };
}

export function showOpenOrders(data) {
  return {
    type: SHOW_OPEN_ORDERS,
    data,
  };
}

export function showMessage(data) {
  return {
    type: SHOW_MESSAGE,
    data,
  };
}

export function toggleSpinner(isToggled, text = '') {
  return {
    type: TOGGLE_SPINNER,
    data: {
      isToggled,
      text,
    },
  };
}

export function showTickerRealTimeSubscribe(data) {
  return {
    type: SHOW_TICKER_REAL_TIME_SUBSCRIBED,
    data,
  };
}

export function setTickerRealTimeSearchTerm(data) {
  return {
    type: SET_TICKER_REAL_TIME_SEARCH_TERM,
    data,
  };
}

export function getRules(data) {
  return {
    type: GET_RULES,
    data,
  };
}

export function toggleAutoTradeByRules(data) {
  return {
    type: TOGGLE_AUTOTRADE_BY_RULES,
    data,
  };
}

export function setTradingEngineResult(data) {
  return {
    type: SET_TRADING_ENGINE_RESULT,
    data,
  };
}
export function startTickerRealTimeAsync() {
  return (dispatch, getState) => {
    const state = getState().app;
    dispatch(toggleSpinner(true, 'Connecting to ticker stream can sometimes take even a minute, please be patient...'));
    dispatch(showTickerRealTimeSubscribe(!state.showTickerRealTimeSubscribed));

    if (state.showTickerRealTimeSubscribed) {
      service.socketTickerRealTime.disconnect().then(c => console.log('disconnected'));
      dispatch(toggleSpinner(false));
      return;
    }

    service.socketTickerRealTime.connect().then(c => console.log('connected'));
    service.socketTickerRealTime.emit('returnTickerRealTime', 'all');
    service.socketTickerRealTime.on('ticker', (data) => {
      if (data.length > 0) {
        dispatch(showTickerRealTimeAsync(data));
        dispatch(toggleSpinner(false));
      }
    });
  };
}
export function showTickerRealTimeAsync(data) {
  return (dispatch, getState) => {
    const actions = [];
    const state = getState().app;
    if (state.autoTrading.isOn) {
      const tradingEngineResult = service.autoTradingEngine.execute(state);
      tradingEngineResult.actionsToBeExecuted.forEach((action) => {
        if (action.action === 'buy') {
          dispatch(buyAsync({
            currencyPair: action.coin,
            amount: action.amount,
            rate: action.lastPrice,
          }));
          if (action.oneShot) actions.push(toggleAutoTradeByRules(false));
        } else if (action.action === 'sell') {
          dispatch(sellAsync({
            currencyPair: action.coin,
            amount: action.amount,
            rate: action.lastPrice,
          }));
          if (action.oneShot) actions.push(toggleAutoTradeByRules(false));
        }
      });
      actions.push(setTradingEngineResult(tradingEngineResult));
    }
    actions.push(showTickerRealTime(data));

    dispatch(batchActions(actions));
  };
}

export function getRulesAsync() {
  return (dispatch, getState) => {
    dispatch(toggleSpinner(true));
    Client.get('getRules').then((res) => {
      if (res.statusCode === 403) {
        dispatch(showMessage(res.body));
      }
      return dispatch(getRules(res.body));
    })
      .catch(err => console.log('err', err))
      .then(() => dispatch(toggleSpinner(false)));
  };
}

export function getBalancesAsync() {
  return (dispatch, getState) => {
    dispatch(toggleSpinner(true));
    Client.get('getBalances').then((res) => {
      if (res.statusCode === 403) {
        dispatch(showMessage(res.body));
      }
      return dispatch(getBalances(
      objectHelper.getNonEmptyArrayValuesFromObject(JSON.parse(res.body))));
    })
      .catch(err => console.log('err', err))
      .then(() => dispatch(toggleSpinner(false)));
  };
}

export function showOpenOrdersAsync() {
  return (dispatch, getState) => {
    dispatch(toggleSpinner(true));
    Client.post('returnOpenOrders', { currencyPair: 'all' })
            .then((res) => {
              if (res.statusCode === 403) {
                dispatch(showMessage(res.body));
              }
              return dispatch(showOpenOrders(res.body));
            })
            .catch(err => console.log('err', err))
            .then(() => dispatch(toggleSpinner(false)));
  };
}

export function buyAsync({ currencyPair, amount, rate }) {
  return (dispatch, getState) => {
    dispatch(toggleSpinner(true));
    Client.post('buy', { currencyPair, amount, rate })
            .then((res) => {
              if (res.statusCode === 403) {
                dispatch(showMessage(res.body));
              }
              return dispatch(showMessage(res.body));
            })
            .catch(err => console.log('err', err))
            .then(() => dispatch(toggleSpinner(false)));
  };
}

export function sellAsync({ currencyPair, amount, rate }) {
  return (dispatch, getState) => {
    dispatch(toggleSpinner(true));

    Client.post('sell', { currencyPair, amount, rate })
            .then((res) => {
              if (res.statusCode === 403) {
                dispatch(showMessage(res.body));
              }
              return dispatch(showMessage(res.body));
            })
            .catch(err => console.log('err', err))
            .then(() => dispatch(toggleSpinner(false)));
  };
}

export function cancelOrderAsync(orderNumber) {
  return (dispatch, getState) => {
    dispatch(toggleSpinner(true));
    Client.post('cancelOrder', { orderNumber })
            .then(res => dispatch(showMessage(res.body)))
            .catch(err => console.log('err', err))
            .then(() => dispatch(toggleSpinner(false)));
  };
}

export function returnTickerAsync() {
  return (dispatch, getState) => {
    Client.get('returnTicker').then((res) => {
      if (res.statusCode === 403) {
        dispatch(showMessage(res.body));
      }
      dispatch(showTicker(JSON.parse(res.body)));
    });
  };
}
