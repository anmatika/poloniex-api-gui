import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { reducer as formReducer } from 'redux-form';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import thunk from 'redux-thunk';
import SocketApi from './utils/SocketApi';
import service from './service';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import appReducer from './reducers/poloniex';
import AutoTradingEngine from './utils/AutoTradingEngine';
import { batchActions, enableBatching } from 'redux-batched-actions';

import './index.css';

const reducer = combineReducers({ app: appReducer, form: formReducer, router });
const createStoreWithMiddleware = applyMiddleware(thunk)(() => createStore(enableBatching(reducer)), window.devToolsExtension ? window.devToolsExtension() : f => f);
const store = createStoreWithMiddleware(reducer);

service.socketTickerRealTime = new SocketApi({ socketPath: 'tickerRealTime' });
service.autoTradingEngine = new AutoTradingEngine();

ReactDOM.render(
  <Provider store={store} >
    <App />
  </Provider>,

document.getElementById('root'));
registerServiceWorker();
