import io from 'socket.io-client';

const host = `${process.env.REACT_APP_HOST}:${process.env.REACT_APP_SERVER_PORT}`;
// console.log('process env react app host', process.env.REACT_APP_HOST);
// console.log('process env react app port', process.env.REACT_APP_SERVER_PORT);

export default class socketAPI {
  constructor({ socketPath }) {
    this.socketPath = socketPath;
  }

  connect() {
    console.log('socketpath', this.socketPath);
    this.socket = io.connect(`${host}/${this.socketPath}`, { forceNew: true });
    return new Promise((resolve, reject) => {
      this.socket.on('connect', () => resolve());
      this.socket.on('connect_error', error => reject(error));
    });
  }

  disconnect() {
    return new Promise((resolve) => {
      this.socket.disconnect(() => {
        this.socket = null;
        resolve();
      });
    });
  }

  emit(event, data) {
    return new Promise((resolve, reject) => {
      if (!this.socket) return reject('No socket connection.');

      return this.socket.emit(event, data, (response) => {
        // Response is the optional callback that you can use with socket.io in every request. See 1 above.
        if (response.error) {
          console.error(response.error);
          return reject(response.error);
        }

        return resolve();
      });
    });
  }

  on(event, fun) {
    // No promise is needed here, but we're expecting one in the middleware.
    return new Promise((resolve, reject) => {
      if (!this.socket) return reject('No socket connection.');

      this.socket.on(event, fun);
      resolve();
    });
  }
}
