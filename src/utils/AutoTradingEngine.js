const AutoTradingEngine = () => {
  const getConditionState = (condition, runningTimeInMinutes, autoTradingCoin, realTimeCoin) => {
    const timeSpanConditionMet = condition.timeSpanMinutes >= runningTimeInMinutes;
    let valueConditionMet = false;
    let priceCutOff;
    if (condition.valueType === '*') {
      priceCutOff = autoTradingCoin.startPrice * condition.value;

      if (condition.valueCompare === '>') {
        valueConditionMet = realTimeCoin.value.lastPrice > priceCutOff;
      } else {
        valueConditionMet = realTimeCoin.value.lastPrice < priceCutOff;
      }
    } else if (condition.valueType === '+') {
      priceCutOff = autoTradingCoin.startPrice + condition.value;
      if (condition.valueCompare === '>') {
        valueConditionMet = realTimeCoin.value.lastPrice > priceCutOff;
      } else {
        valueConditionMet = realTimeCoin.value.lastPrice < priceCutOff;
      }
    } else if (condition.valueType === '-') {
      priceCutOff = autoTradingCoin.startPrice - condition.value;
      if (condition.valueCompare === '>') {
        valueConditionMet = realTimeCoin.value.lastPrice > priceCutOff;
      } else {
        valueConditionMet = realTimeCoin.value.lastPrice < priceCutOff;
      }
    } else {
      throw 'condition.valueCompare missing';
    }
    const isMet = valueConditionMet && timeSpanConditionMet;
    return {
      isMet,
      priceCutOff,
    };
  };
  const execute = (state) => {
    const retObj = {
      buyData: {},
      sellData: {},
      actionsToBeExecuted: [],
    };
    if (state.autoTrading.currencyPairs.length === 0) {
      return retObj;
    }

    state.rules.forEach((rule) => {
      const autoTradingCoin = state.autoTrading.currencyPairs.find(c => c.coin === rule.coin);
      const realTimeCoin = state.tickersRealTime.find(c => c.key === rule.coin);
      const runningTimeInMinutes = new Date(new Date().getTime() - state.autoTrading.startTime.getTime()).getMinutes();
      const priceMovement = ((realTimeCoin.value.lastPrice - autoTradingCoin.startPrice) / autoTradingCoin.startPrice) * 100;
      const startPrice = autoTradingCoin.startPrice;
      const lastPrice = realTimeCoin.value.lastPrice;
      const coin = rule.coin;
      console.log('runningTimeInMinutes', runningTimeInMinutes);
      console.log('price movement %', priceMovement);
      console.log('startPrice', startPrice);
      console.log('lastPrice', lastPrice);

      rule.actions.forEach((action) => {
        action.conditions.forEach((condition) => {
          console.log('coin', coin, 'rule action', action.action);

          // console.log('price cutoff value', priceCutOff);
          const conditionState = getConditionState(condition, runningTimeInMinutes, autoTradingCoin, realTimeCoin);

          console.log('coin', coin, 'conditionsMet', conditionState.isMet);
          if (action.action === 'buy') {
            retObj.buyData.priceMovement = priceMovement;
            retObj.buyData.priceCutOff = conditionState.priceCutOff;
          } else if (action.action === 'sell') {
            retObj.sellData.priceMovement = priceMovement;
            retObj.sellData.priceCutOff = conditionState.priceCutOff;
          }

          if (conditionState.isMet) {
            retObj.actionsToBeExecuted.push({
              coin,
              action: action.action,
              amount: action.amount,
              lastPrice,
            });
          }
        });
        retObj.runningTimeInMinutes = runningTimeInMinutes;
        retObj.priceMovement = priceMovement;
        retObj.startPrice = startPrice;
        retObj.lastPrice = lastPrice;
      });
    });

    return retObj;
  };

  return {
    execute,
  };
};

export default AutoTradingEngine;
