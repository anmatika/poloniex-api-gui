# Building and running polocyrpto requires that you have the server/apikeys.js mapped to your local
# yarn react-scripts build --production
# docker build -t polo . && docker run -td -p 80:80 -p 3001:3001 --name "polo" polo
# docker run -td -p 80:80 -p 3001:3001 -v server/apikeys.js:/usr/share/nginx/html/static/server/apikeys.js -v /etc/nginx/.htpasswd:/etc/nginx/.htpasswd --name "polo" polo

FROM alpine

# Install nginx package and remove cache
#RUN apk add --update nginx && rm -rf /var/cache/apk/*
#FROM mhart/alpine-node:6.7.0
# Install nginx package and remove cache
RUN apk --update add --no-cache nginx && rm -rf /var/cache/apk/*

# Copy basic files
COPY nginx.non-root.conf /etc/nginx/nginx.conf
COPY mime.types /etc/nginx/mime.types
WORKDIR /usr/share/nginx/html
COPY build/static /usr/share/nginx/html/static
COPY build/asset-manifest.json build/favicon.ico build/index.html build/manifest.json build/service-worker.js /usr/share/nginx/html/
COPY server /usr/share/nginx/html/server
RUN ls /usr/share/nginx/html
EXPOSE 443 3001
VOLUME ["/usr/share/nginx/html"]
VOLUME ["/etc/nginx/"]"
VOLUME ["/etc/letsencrypt/live/polo.mylb.co"]

# root user will run 'nginx: master process'
# nobody user will run 'nginx: worker process' as dictated in the nginx.non-root.conf
CMD ["nginx", "-g", "daemon off;"]